
%% ---------------------------------------------------------
%%              Fixed Point
%% ---------------------------------------------------------
\chapter{Fixed Points in Two Unevenly-Pumped Lasers}
Fixed points in for two unevenly-pumped lasers are investigated. The calculation follows line of Tsygankov's work\cite{TsygankovPhDThesis} but treated in refined model\cite{Ray2009} which incorporates three- and four-level laser models. Single-mode iterative map model with equally reflective fiber end conditions $r = r_1 = r_2$ is used. 


Introduction of Even cavity loss and uneven pumping rate make the fixed-point calculation different from the one in Chapter\ref{chap:dynamics}.
Because of the assumptions the iterative map for the field is simplified to 
%
\begin{align} %% single-mode iterative map (reference\cite{twolaser_paper} for the derivation.)
    \begin{pmatrix}[0.7]
        E_1(t+T) \\
        E_2(t+T)
    \end{pmatrix}
    &= 
    r
    \begin{pmatrix}[0.7]
        e^{G_1(t) + i \phi_1^{(m)}} &  0 \\
         0                           & e^{G_2(t) + i \phi_2^{(m)}}
    \end{pmatrix}    
    \begin{pmatrix}[0.7]
         \cos q       & i \sin q \\
        i \sin q       &  \cos q
    \end{pmatrix}
    \begin{pmatrix}[0.7]
        E_1(t) \\
        E_2(t)
    \end{pmatrix}
\end{align}
%
where $q$ was defined by $q \equiv \cos^{-1} (1-2p)$. Gain evolution is dependent on the lasing scheme 
%
\begin{align*}
    G_n(t+T)=
    \begin{cases}
        G_n (t) + \epsilon 
        \left[ w_n (G_{\text{tot}}-G_n(t)) - (G_{\text{tot}}+G_n(t))
         -  2\left(1-e^{-2G_n(t)}\right) |E_n(t)|^2 \right]  
        & \text{for three-level laser} \\
        G_n(t) + \epsilon
        \left[ w_n (G_{\text{tot}}-G_n(t)) - G_n(t) 
         - \left( 1 - e^{-2G_n(t)} \right) |E_n(t)|^2 \right]  
        & \text{for four-level laser}
    \end{cases}
\end{align*}
%
We compute the fixed-point solution. 
Let $E_n(t) = \mathcal{E}_n(t) e^{i \psi _i(t)}$  and $\psi (t) \equiv \psi _1(t)-\psi _2(t)$. Gain variables are changed such that $x(t) = e^{-G_1(t)}$ and $y(t) = e^{-G_2(t)}$. The fixed point solutions $\left(\tilde{\mathcal{E}}_1, \tilde{\mathcal{E}}_2, \tilde{\psi }, \tilde{x}_1,\tilde{x}_2 \right)$ from the map will be computed. There are five independent fixed-point equations. (Although there are six in the dynamical equations but one becomes redundant due to the symmetries  $1\leftrightarrow 2$ and $\psi \to -\psi$.)
%
%
\begin{align}
& \tilde{\mathcal{E}}_1^2 = \frac{r^2}{\tilde{x}_1^2}
   \left( \tilde{\mathcal{E}}_1^2 \cos ^2q +  \tilde{\mathcal{E}}_2^2 \sin ^2q
   + 2 \tilde{\mathcal{E}_1}\tilde{\mathcal{E}}_2
   \sin q \cos  q \sin \tilde{\psi }\right) \label{eq:unevenpump_fixedpoint1}\\
%%
& \tilde{\mathcal{E}}_2^2 = \frac{r^2}{\tilde{x}_2^2}
   \left(\tilde{\mathcal{E}}_2^2 \cos^2 q 
         +\tilde{\mathcal{E}}_1^2 \sin^2 q 
         -2\tilde{\mathcal{E}_1}\tilde{\mathcal{E}}_2
         \sin q \cos  q \sin  \tilde{\psi }\right) \label{eq:unevenpump_fixedpoint2}\\
%%
& \tilde{\mathcal{E}_1} \tilde{\mathcal{E}}_2 e^{i\tilde{\psi}}
    = \frac{r^2}{\tilde{x}_1 \tilde{x}_2}  e^{i \left(\phi _1-\phi _2\right)} 
    \left[ i \left(-\tilde{\mathcal{E}}_1^2+\tilde{\mathcal{E}}_2^2\right) \cos q \sin q 
          + \tilde{\mathcal{E}_1} \tilde{\mathcal{E}}_2 
          \left(\cos^2 q e^{i\tilde{\psi}} + \sin^2 q e^{-i\tilde{\psi}} \right) \right] \label{eq:unevenpump_fixedpoint3}\\
%%
& \tilde{\mathcal{E}_n}^2 = 
   \frac{(w_n+1) \log\left( \tilde{x_n} / x_{n, \text{sat}} \right) }{k (1 - x_n^2)}
   \label{eq:unevenpump_fixedpoint4}
\end{align}
%
%
The last equation holds for both three-level and four-level scheme because the difference is folded in the saturated gain, $\tilde{\mathcal{E}}_n\left(x_{n, \text{sat}}\right)=0$. $k=2$ in three-level and $k=1$ in four-level system. Because $\tilde{\mathcal{E}}_n^2>0$ and $\tilde{\mathcal{E}}_n^2(x)$ is monotonically increasing function, $x_{n, \text{sat}}>0$ is the lower limit of $x_n$. $x_{n, \text{sat}}$ is defined by 
%
%
\begin{align*}
    x_{n, \text{sat}}
    = 
    \begin{cases}
        \exp \left(-\frac{W_n-1}{W_n+1}G_{\text{tot}}\right) & \text{for three level laser} \\
        \exp \left(-\frac{W_n}{W_n+1}G_{\text{tot}}\right) & \text{for four level laser}
    \end{cases}
\end{align*}
%
with $n = 1, 2$. 
From now on, we'll omit tilde from $\left(\tilde{\mathcal{E}}_1,\tilde{\mathcal{E}}_2, \tilde{\psi }, \tilde{x}_1,\tilde{x}_2\right)$ for brevity and consider only fixed point state.

%
Following Tsygankov's simplification, phase advances are assumed equal\footnote{This assertion is very questionable!} in two fibers: $\phi _1=\phi _2$. Then real and imaginary part of the second equation have the form
\begin{align}
    & \mathcal{E}_1 \mathcal{E}_2 \cos \psi  \left(1-\frac{r^2}{x_1x_2}\right)=0 
    \label{eq:weaklink_fixedpoint_real}
    \\
    & \mathcal{E}_1 \mathcal{E}_2 \sin \psi 
    = \frac{r^2}{x_1 x_2} 
    \left( (-\mathcal{E}_1^2 + \mathcal{E}_2^2) \cos q \sin q 
    + \mathcal{E}_1 \mathcal{E}_2 \cos (2 q) \sin \psi  \right)
    \label{eq:weaklink_fixedpoint_imag}
\end{align}
%
%
Laser is assumed active in both fibers so $\tilde{\mathcal{E}_1}$and $\tilde{\mathcal{E}_2}$ are assumed positive definite. Thus, the real part is satisfied if $\cos  \tilde{\psi }=0$ or $x_1x_2=r^2$.




\section{Case A: $\cos \psi \neq 0$}
We first consider the case when $x_1 x_2 = r^2$ holds. Applying the relation to the imaginary part, we have 
%
\[ 
    2 \mathcal{E}_1 \mathcal{E}_2 \sin \psi 
=  \frac{\cos  q }{\sin  q}\left(-\mathcal{E}_1^2+\mathcal{E}_2^2\right)
\]
%
%
Substituting the equation into \eqref{eq:unevenpump_fixedpoint1} and removing $\mathcal{E}_1\mathcal{E}_2 \sin \psi$, we have
%
\begin{align}
    x_1\mathcal{E}_1^2=x_2 \mathcal{E}_2^2
    \label{eq:weaklink_caseA_relation1}
\end{align}
%
From \eqref{eq:unevenpump_fixedpoint4} and \eqref{eq:weaklink_caseA_relation1}, we end up with relation between $x_1$ and $x_2$.
%
\begin{align}
    \frac{x_1}{1-x_1^2}  (W_1 + 1) \log \left(\frac{x_1}{x_{1, \text{sat}}}\right)  
 =  \frac{x_2}{1-x_2^2}  (W_2 + 1) \log \left(\frac{x_2}{x_{2, \text{sat}}}\right)  
 \end{align}
%
If two lasers are equally pumped, $W=W_1=W_2$, only symmetric solution is possible.
\begin{align*}
G_1 &= G_2 = - \log (r) \\
%
\mathcal{E}_1^2 &= \mathcal{E}_2^2 = \frac{(W-1)G_{\text{tot}}+(W+1)\log  r}{k (1-r^2)} \\
%
\psi &= 0, \pi
\end{align*}
%
If pumping rates are uneven, say $W_1>W_2$, then $x_{1, \text{sat}}<x_{2, \text{sat}}$ from definition. 
Also $\frac{x}{1-x^2} \log x$ is positive and monotonically increasing in $x\in (0,1)$. Therefore, inspection of \eqref{eq:weaklink_caseA_relation1} gives unique solution for $W_1 > W_2$ such that $x_1 < x_2$ (that is, $G_1 > G_2$) and $\mathcal{E}_1 > \mathcal{E}_2$.



%%===================================================
%%     Case B:   \cos( \psi ) = 0
%%
\section{Case B: $\cos \psi = 0$}
%
Let $g_0\equiv \sin  \psi =\pm 1$. Then 
%
\begin{align*}
    \tilde{\mathcal{E}}_1^2 = \frac{r^2}{\tilde{x}_1^2} 
    \left(\mathcal{E}_1 \cos  q +g_0 \mathcal{E}_2 \sin  q \right)^2 \\
%%
    \tilde{\mathcal{E}}_2^2 = \frac{r^2}{\tilde{x}_2^2}
    \left( \mathcal{E}_1 \cos  q  - g_0 \mathcal{E}_2 \sin  q \right)^2
\end{align*}
%
Introduce $g_1=\pm 1, g_2=\pm 1.$
%
\begin{align*}
g_1 &= \frac{r}{x_1}\left(\cos  q+g_0\frac{\mathcal{E}_2}{\mathcal{E}_1}\sin  q \right) \\
g_2 &= \frac{r }{x_2}\left(\cos  q-g_0\frac{\mathcal{E}_1}{\mathcal{E}_2}\sin  q \right)
\end{align*}
%
After rearrangement of terms we have, 
%
\begin{align}
    & g_0\frac{\mathcal{E}_2}{\mathcal{E}_1}\sin  q=g_1\frac{x_1}{r} - \cos q,  \label{eq:weaklink_caseB_01}\\
    & -g_0\frac{\mathcal{E}_1}{\mathcal{E}_2}\sin  q=g_2\frac{x_2}{r} - \cos  q.
    \label{eq:weaklink_caseB_02}
\end{align}
%
Applying the property $g_0^2=1$ for \eqref{eq:weaklink_fixedpoint_imag}, 
%
\begin{align*}
1 = \frac{r^2}{x_1 x_2} \left[ \left( - \frac{\mathcal{E}_1}{\mathcal{E}_2} + \frac{\mathcal{E}_2}{\mathcal{E}_1} \right)
  g_0\cos  q \sin  q + \left(\cos ^2 q - \sin ^2 q \right) \right]
\end{align*}
%
Removing $g_0$ term by the substitution of \eqref{eq:weaklink_caseB_01} and \eqref{eq:weaklink_caseB_02}.
%
\begin{align*}
    1=\frac{r^2}{x_1x_2}\left[\left(g_1\frac{x_1}{r}+g_2\frac{x_2}{r}\right)\cos  q-1 \right]
\end{align*}
%
%
With rearrangement we have resulting equation for $x_1$and $x_2$.
%
%
\begin{align*}
    & \frac{x_1 x_2}{r^2} - g_1 \frac{x_1}{r} \cos q = g_2 \frac{x_2}{r} \cos q - 1 \\
    & g_1 x_1 = r\frac{g_2x_2\cos  q-r}{g_1x_2-r \cos  q} \\
    & g_1 x_1- r \cos  q = r\frac{\left(g_2-g_1\right)x_2 \cos  q-r \sin ^2q}{g_2x_2-r \cos  q}
\end{align*}
%
On the other hand, from \eqref{eq:weaklink_caseB_01} divided by \eqref{eq:weaklink_caseB_02}, 
%
\begin{align*}
- \left(\frac{\mathcal{E}_1}{\mathcal{E}_2}\right)^2=\frac{g_2x_2-r \cos  q}{g_1x_1-r \cos  q }=\frac{1}{r}\frac{\left(g_1x_2-r \cos  q\right)\left(g_2x_2-r
\cos  q\right)}{\left(g_2-g_1\right)x_2 \cos  q-r \sin ^2q}
\end{align*}
%
This $\left(\mathcal{E}_1/\mathcal{E}_2\right)^2$ term may be also obtained from gain equations.
%
\begin{align*}
\left(\frac{\mathcal{E}_1}{\mathcal{E}_2}\right)^2=\frac{W_1+1}{W_2+1}\frac{1-x_2^2}{1-x_1^2}\frac{\log  \left(x_1/x_{1, \text{sat}}\right)}{\log \left(x_2/x_{2, \text{sat}}\right)}
\end{align*}
%
%
Thus, we end up having two equations for $x_1$ and $x_2$.
%
\begin{align}
& x_1=r\frac{g_2x_2 \cos  q-r}{x_2-g_1r \cos  q} \label{eq:weaklink_fixedpoint_caseB1}\\
& \frac{W_1+1}{W_2+1} \frac{1-x_2^2}{1-x_1^2} \frac{\log  \left(x_1/x_{1, \text{sat}}\right)}{\log  \left(x_2/x_{2, \text{sat}}\right)}
= \frac{(g_1x_2-r \cos  q) (g_2x_2-r \cos  q)}{(g_1-g_2) x_2 r \cos  q+r^2 \sin ^2q} 
\label{eq:weaklink_fixedpoint_caseB2}
\end{align}
%
We are going to examine the number of solutions $\left(x_1, x_2\right)$ satisfying these two equations. Notice that only the difference from calculation by Tsygankov is the left-hand side of the equation \eqref{eq:weaklink_fixedpoint_caseB2}. In Tsygankov's case \cite{TsygankovPhDThesis}
%
\[
L_{\text{Tsygankov}}\left(x_1, x_2\right) = 
\frac{\left(1-x_2\right)x_2}{\left(1-x_1\right)x_1}\frac{\log  \left(x_1/x_{1, \text{sat}}\right)}{\log
 \left(x_2/x_{2, \text{sat}}\right)}
 \]
%
%
while our LHS of \eqref{eq:weaklink_fixedpoint_caseB2} is
%
\[
L (x_1, x_2) = 
\frac{\log  \left(x_1/x_{1, \text{sat}}\right)}{\log \left(x_2/x_{2, \text{sat}}\right)}.
\]
%
%
Since gain $G_i$ are assumed positive definite, $x_1, x_2\in (0, 1)$. Signs of $L_{\text{Tsygankov}}\left(x_1,x_2\right)$ and $L\left(x_1,x_2\right)$ are determined from signs of $\log  \left(x_1/x_{1, \text{sat}}\right)$ and $\log  \left(x_2/x_{2, \text{sat}}\right)$.
We'll show that the number of fixed points is determined solely by the sign of the $L(x_1, x_2)$ function, which is analogous to Tsygankov's calculation. 

Now, consider three different cases (1) $g_1=g_2=+1$, (2) $g_1=-g_2=\pm 1$, (3) $g_1=g_2=-1$.



%-----------------------------------------------------------
%             Case:  B-1
%
\subsection{Case B-1: $g_1 = g_2 = 1$}
%
\begin{align}
    & x_1 = f(x_2) = r\frac{x_2 \cos  q-r}{x_2-r \cos  q} \\
    %%
    & L\left(x_1,x_2\right)
    = \frac{(W_1 + 1)}{(W_2 + 1)} \frac{ h (x_1,x_{1\text{sat}})}{h (x_2, x_{2\text{sat}})} \\
    %
    & R\left(x_2\right)=\frac{\left(x_2-r \cos  q\right)^2}{r^2 \sin ^2q}.
\end{align}
%
%
with 
\begin{align*}
    h(x, x_\text{sat}) \equiv \frac{\log \left(x / x_{\text{sat}} \right)}{1 - x^2}.
\end{align*}
which is monotonically increasing function of $x \in (0, 1)$ and 
$h (0^+, x_{\text{sat}}) = -\infty$, $h (x_{\text{sat}}, x_{\text{sat}}) = 0$, and $h \left(1^-,x_{\text{sat}}\right) = +\infty$. Note that the function $y=f(x)$ introduced in the first equation is symmetric about $y = x$.
%
%
\begin{align*}
    x_2 = f^{-1}(x_1) = f(x_1) = r \frac{x_1 \cos q - r}{x_1 - r \cos q}
\end{align*}
%
%
As Figure~\ref{fig:weaklink_case_b1} illustrates, the function $f(x)$ maps $x_1$ to $x_2$ such that the graph $x_2 = f(x_1)$ starts from $(0, \alpha )$, monotonically increases up to $(\beta ,1)$, appears at $(\alpha ,0)$ after a gap, then monotonically increases again up to $(1,\beta )$. Here $\alpha$ and $\beta$ are constants given by 
%
\begin{align*}
    \beta &= f(1) =r \frac{\cos q - r}{1 - r \cos q},\\
    %
    \alpha &= f(0) =\frac{r}{\cos q}.
\end{align*}
%
%
If $\cos q \leq r$, then $x_1 \notin (0,1)$ for $x_2 \in (0,1)$ and vice versa. Therefore $\cos q > r$ is the requirement so that a fixed point solution exists. With this requirement satisfied, we have order relations $0 < \beta < r \cos q < \alpha < 1$ so the gap in the graph $y=f(x)$ always exists.
%
%
\begin{figure}[hb]
    \centering
    \includegraphics{pdf/weaklink_fixedpoint_caseB1.pdf}
    \caption{Case B-1: Relation between $x_1 = e^{-G_1}$ and $x_2 = e^{-G_2}$ in steady state.}
    \label{fig:weaklink_case_b1}
\end{figure}
%
The number of fixed point solution is determined from relative values of $x_{1, \text{sat}}$ and $x_{2, \text{sat}}$ with respect to $0 < \beta < \alpha < 1$. There are six possible cases.
%
\begin{enumerate}
    \item $0 < x_{1, \text{sat}} < \beta$ and  $0 < x_{2, \text{sat}} < \beta$
    \item $0 < x_{1, \text{sat}} < \beta$ and  $\beta < x_{2, \text{sat}} < \alpha$
    \item $0 < x_{1, \text{sat}} < \beta$ and  $\alpha < x_{2, \text{sat}} < 1$    
    \item $\beta < x_{1, \text{sat}} < \alpha$ and $\beta < x_{2, \text{sat}} < \alpha$
    \item $\beta < x_{1, \text{sat}} < \alpha$ and $0 < x_{2, \text{sat}} < \beta$
    \item $\beta < x_{1, \text{sat}} < \alpha$ and $0 < x_{2, \text{sat}} < \beta$    
\end{enumerate}
%
%


% -------------------------------------------------------------
%        B-1-1
%
\subsection{Case B-1-1: $0 < x_{1,\mathrm{sat}} < \beta$ and 
$0 < x_{2,\mathrm{sat}} < \beta$} 
%
%
\begin{table}[ht]
\centering
\begin{tabular}{c|c|c|c|c}
  $x_1$ & $x_2$ & $L_{\text{numer}}$ & $L_{\text{denom}}$ & $L$ \\
\hline \hline
$ 0^+   $ & $ \alpha ^+$ & $-\infty $ & $+$ & $-\infty $ \\ \hline
$ x_{1\text{sat}}$ & $f\left(x_{1\text{sat}}\right)$ & $0$ & $+$ & $0$ \\ \hline
$ \beta $ & $1^-$ & $+$ & $+\infty $ & $0$ \\ \hline
   &   &   &   &   \\ \hline
$ \alpha $ & $0$ & $+$ & $-\infty $ & $0$ \\ \hline
$ f\left(x_{2 \text{sat}}^-\right)$ & $x_{2 \text{sat}}^-$ & $+$ & $-0$ & $-\infty $ \\
$ f\left(x_{2\text{sat}}^+\right)$ & $x_{2\text{sat}}^+$ & $+$ & $+0$ & $+\infty $ \\ \hline
$ 1^-$ & $\beta ^-$ & $+\infty $ & $+$ & $+\infty $ \\
\end{tabular}
\caption{Case: B-1-1}
\label{tab:case_b_1_1}
\end{table}
%
%
From the table \ref{tab:case_b_1_1}, together with $x\geq x_{\text{sat}}$, we can say that $L\left(x_1,x_2\right)$ has a local positive maximum in $x_2 \in (f (x_{1\text{sat}}), 1)$ and a local positive minimum in $x_2\in \left(x_{2\text{sat}},\beta \right)$. Therefore, depending on the shape of $R(x_2)$, number of fixed-point solutions varies from zero to four.



% -------------------------------------------------------------
%        B-1-2
%
\subsection{Case B-1-2: $0 < x_{1,\mathrm{sat}} < \beta$ and 
$\beta < x_{2,\mathrm{sat}} < \alpha$} 
%
%
\begin{table}[ht]
\centering
\begin{tabular}{c|c|c|c|c}
$ x_1$ & $x_2$ & $L_{\text{numer}}$ & $L_{\text{denom}}$ & $L$ \\
\hline \hline
$ 0$ & $\alpha $ & $-\infty $ & $+$ & $-\infty $ \\
\hline
$ x_{1\text{sat}}$ & $f\left(x_{1\text{sat}}\right)$ & $0$ & $+$ & $0$ \\
\hline
$ \beta $ & $1$ & $+$ & $+\infty $ & $0$ \\
\hline
   &   &   &   &   \\
\hline
$ \alpha $ & $0$ & $+$ & $-\infty $ & $0$ \\
\hline
$ 1$ & $\beta $ & $+\infty $ & $-$ & $-\infty $ \\
\end{tabular}
\caption{Case: B-1-2}
\label{tab:case_b_1_2}
\end{table}
%
%
The table\ref{tab:case_b_1_2} shows, together with $x\geq x_{\text{sat}}$, that $L\left(x_1,x_2\right)$ takes a local positive maximum at $x_2 \in \left(f\left(x_{1\text{sat}}\right),1\right)$. There are zero to two possible fixed-point solutions.



% -------------------------------------------------------------
%        B-1-3a
%
\subsection{Case B-1-3a: $0 < x_{1,\mathrm{sat}} < \beta$ and 
$\alpha < x_{2,\mathrm{sat}} < f(x_{1,\mathrm{sat}})$} 
%
\begin{table}[ht]
\centering
\begin{tabular}{c|c|c|c|c}
$ x_1$ & $x_2$ & $L_{\text{numer}}$ & $L_{\text{denom}}$ & $L$ \\
\hline \hline
$ 0$ & $\alpha $ & $-\infty $ & $-$ & $+\infty $ \\
\hline
$ f\left(x_{2,\text{sat}}\right)$ & $x_{2,\text{sat}}^-$ & $-$ & $-0$ & $+\infty $ \\
\hline
$ f\left(x_{2 \text{sat}}\right)$ & $x_{2 \text{sat}}^+$ & $-$ & $+0$ & $-\infty $ \\
\hline
$ x_{1\text{sat}}$ & $f\left(x_{1\text{sat}}\right)$ & $0$ & $+$ & $0$ \\
\hline
$ \beta $ & $1$ & $+$ & $+\infty $ & $0$ \\
\hline
   &   &   &   &   \\
\hline
$ \alpha $ & $0$ & $+$ & $-\infty $ & $0$ \\
\hline
$ 1$ & $\beta $ & $+\infty $ & $-$ & $-\infty $ \\
\end{tabular}
\caption{Case: b-1-3a}
\label{tab:case_b_1_3a}
\end{table}
%
From the table \ref{tab:case_b_1_3a}, together with $x\geq x_{\text{sat}}$, $L$ has a local positive maximum in $x_2\in \left(f\left(x_{1\text{sat}}\right),1\right)$. Therefore, depending on the shape of $R\left(x_2\right)$, positive parabolic curve in the domain $x_2\in (0,1)$, either zero or two solutions is possible.




% -------------------------------------------------------------
%        B-1-3b
%
\subsection{Case B-1-3b: $0 < x_{1,\mathrm{sat}} < \beta$ and 
$f(x_{1,\mathrm{sat}}) < x_{2,\mathrm{sat}} < 1$} 
%
%
\begin{table}[ht]
\centering
\begin{tabular}{c|c|c|c|c}
$ x_1$ & $x_2$ & $L_{\text{numer}}$ & $L_{\text{denom}}$ & $L$ \\
\hline \hline
$ 0$ & $\alpha $ & $-\infty $ & $-$ & $+\infty $ \\
\hline
$ x_{1 \text{sat}}$ & $f\left(x_{1\text{sat}}\right)$ & $0$ & $-$ & $0$ \\
\hline
$ f\left(x_{2,\text{sat}}\right)$ & $x_{2,\text{sat}}^-$ & $+$ & $-0$ & $-\infty $ \\
\hline
$ f\left(x_{2 \text{sat}}\right)$ & $x_{2 \text{sat}}^+$ & $+$ & $+0$ & $+\infty $ \\
\hline
$ \beta $ & $1$ & $+$ & $+\infty $ & $0$ \\
\hline
$  $ & $ $ & $ $ & $ $ & $ $ \\
\hline
$ \alpha $ & $0$ & $+$ & $-\infty $ & $0$ \\
\hline
$ 1$ & $\beta $ & $+\infty $ & $-$ & $-\infty $ \\
\end{tabular}
\caption{Case: b-1-3b}
\label{tab:case_b_1_3b}
\end{table}

From the table, together with $x \geq x_{\text{sat}}$, $L\left(x_1,x_2\right)$ monotonically decreases from $+\infty$ to 0 in $x_2\in \left(x_{2,\text{sat}},1\right)$. $R\left(x_2\right)$ is parabolic , there is one crossing (one solution) in the range.



% -------------------------------------------------------------
%        B-1-4
%
\subsection{Case B-1-4: $\beta < x_{1,\mathrm{sat}} < \alpha$ and 
$0 < x_{2,\mathrm{sat}} < \beta$} 
%
%
This is a mirror image of Case 2 with $1 \leftrightarrow 2$. Taking care of the fact $L=(L \text{with }1\leftrightarrow 2\text{})^{-1}$, we can say that $L\left(x_1,x_2\right)$ takes a local positive minimum in $x_1\in \left(f\left(x_{2\text{sat}}\right),1\right)$, implying that $x_2\in \left(x_{2,\text{sat}},\beta \right)$. 




% -------------------------------------------------------------
%        B-1-5a
%
\subsection{Case B-1-5a: $\alpha < x_{1,\mathrm{sat}} < f(x_{2,\mathrm{sat}})$ and 
$0 < x_{2,\mathrm{sat}} < \beta$} 
%
%
This is also a mirror image of Case 3a with $1\leftrightarrow 2$. From the fact $L=(L \text{with }1\leftrightarrow 2\text{})^{-1}$, $L\left(x_1,x_2\right)$ takes a local positive minimum in $x_1\in \left(f\left(x_{2\text{sat}}\right),1\right)$, implying that $x_2\in \left(x_{2,\text{sat}},\beta \right)$. 



% -------------------------------------------------------------
%        B-1-5b
%
\subsection{Case B-1-5b: $f(x_{2,\mathrm{sat}}) < x_{1,\mathrm{sat}} < 1$ and 
$0 < x_{2,\mathrm{sat}} < \beta$} 

%
This is mirror image of Case 5b with $1\leftrightarrow 2$. From the fact $L=(L \text{with }1\leftrightarrow 2\text{})^{-1}$, $L\left(x_1,x_2\right)$ monotonically increases from 0 to +Infinity in $x_1\in \left(x_{1,\text{sat}},1\right)$, implying that $x_2\in \left(f\left(x_{1,\text{sat}}\right),\beta \right)$.





% -------------------------------------------------------------
%        B-1-6
%
\subsection{Case B-1-6: $\beta <  x_{1,\mathrm{sat}}$ and 
                        $\beta <  x_{2,\mathrm{sat}}$} 
%
Because $x_{1,\text{sat}}<x_1$ and $\beta <x_{1\text{sat}}$, $x_1>\beta$. Then $x_2=f\left(x_1\right)$ and structure of $f$ imposes $x_2<\alpha$. But it contradicts the restriction for $x_2$: $\beta <x_{2\text{sat}}<x_2$. Therefore there is no fixed point solution in this case.





% ====================================================================
%        B-2
%
\subsection{Case B-2: $g_1 = - g_2 = \pm 1$} 
%
\begin{align*}
    & x_1 = f_A\left(x_2\right)= r\frac{x_2 \cos  q\pm r}{r \cos  q\mp x_2} \\
    %
    & \frac{W_1 + 1}{W_2 + 1} \frac{1 - x_2^2}{1 - x_1^2} 
    \frac{\log \left(x_1/x_{1, \text{sat}}\right)}{\log \left(x_2/x_{2, \text{sat}}\right)}
    = \frac{r^2 \cos^2 q - x_2^2}{r^2 \sin^2 q \pm 2 x_2 r \cos q}
\end{align*}
%
Let inverse of $f_A$ be $f_B$. 
%
\[
x_2 = f_B(x_1) = r \frac{x_1 \cos  q\mp r}{r \cos  q\pm x_1}
\]
%
Unlike the previous case $g_1=g_2=+1$, the function is no longer self-inverse. The graph is illustrated in \ref{fig:case_b_2} where two segments of curve correspond to upper and lower signs of $g_1=-g_2=\pm1$.
%
%
\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{pdf/weaklink_fixedpoint_caseB2.pdf}
    \caption{Relation between $x_1$ and $x_2$ for $g_1=-g_2=+1$ (blue curve) and $g_1=-g_2=-1$ (red curve).}
    \label{fig:case_b_2}
\end{figure}
%

When $g_1=-g_2=+1$, introduce analogous constants as case B-1: $\alpha = f_B(0)$ and $\beta = f_B(1)$ where domain of $x_1$ is $x_1 \in (0, \beta)$. Thus, part of the statements in the case B-1-1 and all the statements in the case B-1-2 and B-1-3 are valid for $g_1=-g_2=+1$ with current definition of $\alpha$ and $\beta$. 

Similarly, for (b) $g_1=-g_2=-1$, domain of $x_1$ is $x_1 \in (\alpha, 1)$  and part of the statements in the case B-1-1 and all the statement in the case B-1-4 and B-1-5 are valid.



% ====================================================================
%        B-3
%
\subsection{Case B-3: $g_1 = g_2 = - 1$} 
%
Then $x_1$ and $x_2$ are related by 
%
\[
x_1 = f\left(x_2\right) = - r \frac{x_2 \cos q + r}{x_2 + r \cos  q} < 0
\]
%
%
for $x_2 \in (0,1)$. The map violates $0 < x_{1 \text{sat}} < x_1$ so there is no fixed-point solution.


