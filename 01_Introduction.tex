% !TEX root = __main.tex

%% =====================================================================
%% Thesis --- Introduction
%%
%%      2013-06    moved to bitbucket for revision management
%%      2013-06-20 totally revised
%%	    2013-06-14	
%%		2012-11-09 modified	
%%      2010-10-20 started
%% =====================================================================

\chapter{Introduction}
\label{chap:intro}

%% =========================================================
%% # High Power Laser
%%
%% - power 
%% - beam quality
%% - radiance (brightness)
%% - robustness
%%
%% array 
%%    CBC and WBC **increases** radiance while incoherent addition does not [Fan, Leger].
%%    But I'm not going to discuss wavelength beam combinig (WBC). Just mention WBC in 
%%    context of scaling of radiance. 
%%
%%  Source: Fan2005, Richardson2010
%% =========================================================
\section{High Power Lasers}
Research and development of high-power lasers is moving toward improvement of power, 
beam quality, radiance, efficiency, and robustness. Power is net energy emitted as a laser beam.
Beam quality, when evaluated in $M^2$, stands for beam divergence during 
the propagation in space compared to the ideal Gaussian beam. 
Radiance, also called ``brightness'' in laser community means a combined measure
of field strength and beam divergence, therefore, in unit of ``power per unit area per solid angle
 ([$\mathrm{W /(m^2 sr)}$])''. A robust system operates steadily without external control 
 in a 
 disturbing 
environment.\footnote{From dynamical-systems view, robustness is a size of 
parameter space for existence of attractors, and/or basin of attraction for steady state.}

High-power and high-quality beam is required of directed-energy applications, 
such as industrial cutting and wielding, laser weapons, laser-beamed propulsions, etc.
Good beam quality minimizes divergence loss during the transmission.
Chemical lasers, such as oxygen iodine laser and deuterium fluoride laser,
are often employed for directed-energy purposes 
because of relative ease of high-power generation. 
Researchers have achieved up to ``Megawatt-class'' 
continuous-wave (CW) laser nowadays with beam quality close to ideal limit.


High-radiance beam is needed to ignite fusion reactions\cite{} and 
possibly to accelerate particles in next-generation LHC\cite{Mourou2013}.
Field strength, rather than total power, is particularly of interest for these purposes.

%--- Fiber starts here
Fiber laser is one of the best platform for both in power and beam quality
because of its built-in waveguide and ease of heat removal due to large surface-volume 
ratio\cite{Richardson2010}. 
Thermal effects, however, sets the limit to radiance, or field strength. 
Single-mode (single-transverse mode) fiber, producing the best beam quality, 
suffers from this nonlinearity the most because of small core area.
High-power fiber lasers are therefore operated in array to scale up power and radiance.
Arrays also provide better efficiency because collection of moderately-powered 
laser sources are typically more efficient than a single high-powered source. %% [Why?]




%% =========================================================
%%       Incoherent and Coherent Beam Combining
%% =========================================================
\section{Why Combine Beams Coherently?}
Incoherent combination, or bunch of collimated beams without phase controls, 
can scale power proportional to the number $N$ of array elements. 
Good beam quality with robust operation are also attainable with incoherent 
combining\cite{Sprangle2009}. Indeed, the most powerful fiber laser so far came from 
this incoherent scheme, reaching 10 kW\cite{IPG-Photonics2009} in single-mode operation.

Then what is the motivation for combining beams coherently?
The answer is in scaling of radiance: when lasers are coherently combined, 
radiance scales up in proportion to the number of array elements $N$. 
On the other hand, incoherent combining \emph{cannot} produce radiance of light 
greater than its constituent element\cite{FanBookChapter}.


Conservation of radiance for incoherent sources may be understood from 
examples\footnote{``brightness theorem''\cite{LegerBookChapter} 
backs up the idea of conservation of radiance. 
But it is typically discussed within ray optics.}. 
Imagine collection of laser pointers. When they are beamed in parallel 
(Fig.\ref{fig:incoherent_sources_parallel}), they may be treated as a single light source with 
large diameter. Although total power is scaled, both power density and beam quality 
stay the same as these of a single source. If beams are set to focus at a point (Fig. 
\ref{fig:incoherent_sources_targeted}), power-density is scaled up at the focal point 
but beam divergence is also inevitably increased, yielding no increase in 
radiance from the parallel case.

% In terms of on-axis field intensity, coherent scheme scales in proportion to 
%%%$N^2$,
% while incoherent scales $N$. Scaling of $N^2$ in retains width of beam profile while $N$...
% [EDIT: Think I should explain this using random variables.]


\section{Active and Passive Techniques}
To add multiple laser elements coherently we need to tune optical phases to match them. 
This technique is already in practical use in radio-frequency domain 
as phased-array radars (Figure \ref{fig:phased_array_radar}) but
optics application is more difficult because scale of wavelength is way shorter: 
lasers are in wavelengths scale $\lambda \sim 1 \mu m$ while radars are of $\lambda \simeq 10 cm$.


%% Active phase control
%%     Anderegg2006 ... good example for active phase controls
%%     Shay2007     ... improvement by removing reference beam
One straightforward approach for coherent addition is to install phase modulators 
to each light path and control them individually by integrating with sensors and electronic 
feedback circuits. We call such techniques as ``active'' because phases are directly 
manipulated. This architecture has produced power as much as 470 W\cite{Anderegg2006}.
The innate drawback of the active phase control is complexity of the architecture 
due to integration of high-speed detectors and signal-processing units.


%% Passive phase control
%% 		Fan2005      ... review of both coherent beam combinig and wavelength beam combining
%% 		Corcoran2009 ... review of coherent beam combining
Passive phase control, on the other hand, attempts to match phases 
by selectively activating modes\cite{Fan2005, Corcoran2009}. Spatial filtering or coupling 
(e.g. tapered fused coupler, Talbot cavity, self-Fourier cavity) is typically 
introduced to achieve synchronized excitation of ``phase-locked''\footnote{People in 
beam-combining community often use terms ``phase locking'' or ``mode locking'' 
to represent successful phase controls of waves with the \emph{same frequency}. 
But, phase- or mode-locking usually means pulse 
generation\cite{SiegmanTextbook}
by adjusting phase of waves with \emph{different frequencies}. So, ``phase locking'' in 
beam combining does not necessary mean pulse generations. Please be careful.} modes. 
The passive scheme offers simpler architecture without phase modulators and sensors. 
It is also expected to operate more robustly due to its self-organizing nature.



%% ===========================================================
%%       Recent Breakthrough with Fiber Lasers Array
%% ===========================================================
\section{Passive Coherent Combining of Fiber Lasers}
Passive coherent beam combining of lasers has been of a long-standing research 
interest\cite{Fan2005}, especially with semiconductor laser 
array\cite{BotezBookChapter}. 
Recent experiments, however, have shown that coherent addition is 
successfully achieved with fiber lasers\cite{Shirakawa2002, Simpson2002}, 
even without careful control of parameters\cite{Bruesselbach2005} 
such as optical path lengths and polarization.

% --- wow! 
These successes were surprising because diode lasers, major platform before fibers,
were struggling for long time. Even more, fiber lasers generally contain 
much more oscillations (i.e. axial modes) than diode lasers. 
It was counterintuitive that phase-matching became easier
in systems with larger the degrees of freedom\footnote{This paradoxical 
situation reminds me of the word of a samurai master; 
``if you hope to stretch one, you've got to shrink it, first. 
If you hope to get one, you've got to give it away, first. 
Therefore, if you want to open one, you've got to close it, first.''\cite{Shigurui}.}.


% ---------
% Theoretical understanding is shaky..
Even more unclear to me personally is understanding of passive coherent beam combining itself.
It seems tacit knowledge like ``lowest-loss mode excitation'' is shared among 
laser researchers; modes suffering less loss are activated more, and vice 
versa.

Basov \emph{et al} carried out a concrete analysis in context of 
diffraction-coupled lasers\cite{Basov1965} and they has shown that supermodes
(i.e. ``mode'' permeating to multiple cavities) with lower power loss than 
those of uncoupled lasers are excited stably. But the analysis is too specific
 to be widely applied to other coupled-laser systems.
 
Due to the widely-accepted knowledge, ``cold-cavity analysis'' are prevalent 
in laser papers and they take laser system as a hollow cavity from 
which light modes are dissipating. This operation is surprising or even 
unacceptable as a physicist because it is like treating nonequilibrium system 
(or dissipative system in sense of Prigogine) as damped linear oscillators.

Even usage of ``modes'' and ``loss'' are not well defined, or context-dependent.
But spacial filtering techniques, mentioned in previous section, came from this 
understanding of the low-loss activation mechanism and they work fine in practice.
 
Although our goal is in understanding of passive coherent combining in general,
we are going to focus on two schemes. One is all-fiber combining approach\cite{Kozlov1999},
which combines light within fiber array, and the other is multi-output system 
which combines beams in the far field\cite{Ray2012Unpublished} 
(Figure~\ref{fig:combining_diagram}). 
%
%%========================================================
% Coherent combining Sketches
\begin{figure}
    \centering
    \begin{subfigure}[b]{7cm}
   	  \includegraphics[width=\textwidth]{pdf/figure_combining_diagram_passive_single.pdf}
      \caption{Single-output scheme}
    \end{subfigure}
    %
    \begin{subfigure}[b]{7cm}
      \includegraphics[width=\textwidth]{pdf/figure_combining_diagram_passive_multiple.pdf}
      \caption{Multi-output scheme}
    \end{subfigure}
    \caption{Approaches for passive coherent beam combining. Retrieved from 
    \cite{Fan2005}.}
    \label{fig:combining_diagram}
\end{figure}
%%========================================================
All-fiber and far-field combining have exclusive pros and cons.
Former scheme is simpler because it does not require beam collimation. 
On the other hand, output power is limited by thermal effects in fiber-core media 
because combined light must pass through a fiber. And far-field scheme shows
totally opposite nature.



%% ===========================================================
%%       Structure of the Thesis
%% ===========================================================
\section{Structure of the Thesis}
Chapter \ref{chap:laserphysics} reviews physics of optical and laser components 
so that we are ready to create a dynamical model for coherent beam combining 
in Chapter \ref{chap:models}. The dynamical model is used to discuss all-fiber, 
linearly-polarized system in Chapter \ref{chap:dynamics}. 
Importance of polarization controls is discussed later in Chapter \ref{chap:polarization}.
Far-field combining system motivated by weak-link 
synchronization\cite{Tsygankov2006}
is described in Chapter \ref{chap:weaklink}. 
Chapter \ref{chap:coupler} is little bit off the story but it will cover 
theory and algorithm of composite coupler construction, 
which are of both mathematical and experimental interest.

